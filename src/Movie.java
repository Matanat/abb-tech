class Movie extends Media {
    @Override
    public void play() {
        System.out.println("Playing movie: " + getTitle());
        System.out.println("Duration of the movie: " + getDuration());
    }
}