class Music extends Media {
    @Override
    public void play() {
        System.out.println("Playing music: " + getTitle());
        System.out.println("Duration of the music: " + getDuration());
    }
}