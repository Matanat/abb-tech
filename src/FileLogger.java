class FileLogger implements Logger {
    @Override
    public void logInfo(String message) {

        System.out.println("Writing Infoto a file: " + message);
    }
    @Override
    public void logWarning(String message) {

        System.out.println("Writing Warning to a file: " + message);
    }
    @Override
    public void logError(String message) {

        System.out.println("Writing Error to a file: " + message);
    }
}
